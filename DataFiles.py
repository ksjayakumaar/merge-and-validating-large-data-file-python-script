import threading, time, glob, os
import pandas as pd, sys, multiprocessing
from multiprocessing import Manager, Process

class CSV_Reading:

    def __init__(self):

        print ("Reading static file(s).......")
        self.NROWS = 1000000
        self.MAX_THREAD_COUNT = 10

    def processingFile(self, fileNames, myList):

        start = time.time()
        multiprocessing.active_children()
        print("Total data file(s): {} ".format(len(fileNames)))
        tmpCheck = [Process(target=self.threadFile, args=(tmpFile, myList)) for tmpFile in fileNames]
        [tmpProcess.start() for tmpProcess in tmpCheck]

        while True:
            if any(proc.is_alive() for proc in tmpCheck):
                time.sleep(1)
                continue
            else:
                [tmpProcess.join() for tmpProcess in tmpCheck]
                break

        [tmpProcess.terminate() for tmpProcess in tmpCheck]
        print("\nTotal time taken: {:.0f} sec(s): ".format(time.time() - start))

    def threadFile(self, tfileName, myList):

        mgr = Manager()
        threadList = mgr.list()
        tList = []

        print ("Process ID: {}, Verify & Validate the file, {}".format(os.getpid(), tfileName))
        start = time.time()
        tmp = pd.read_csv(tfileName, low_memory=False, iterator=True)
        print("Reading file time: {:.2f} sec(s)".format(time.time() - start))

        try:
            while threading.active_count() <= self.MAX_THREAD_COUNT:
                start = time.time()
                tmpThread = threading.Thread(target=self.readCSV, args=(tmp.get_chunk(self.NROWS), threadList))
                tmpThread.start()
                tList.append(tmpThread)
                print("Time taken for chunk file: {:.2f} sec(s)".format(time.time() - start))
        except Exception:
            pass

        start = time.time()
        [tmpTrd.join() for tmpTrd in tList]
        print("Joining of all thread time: {:.2f} sec(s)".format(time.time() - start))

        tmpUpdate = threading.Thread(target=self.updateValues, args=(myList, threadList))
        tmpUpdate.start()
        tmpUpdate.join()

    def updateValues(self, myList, threadList):

        tmpProcessName = threading.current_thread().name
        tmpProcessId = threading.get_ident()
        start = time.time()
        [myList.insert(0, tmp) for no, tmp in enumerate(threadList)]
        print("{}:{}_Loading list time: {:.2f} sec(s)".format(tmpProcessName, tmpProcessId, time.time() - start))

    def readCSV(self, tmpReference, threadList):

        start = time.time()
        rowValues = tmpReference.iloc[:,0]
        tmpProcessName = threading.current_thread().name
        tmpProcessId = threading.get_ident()
        print("{}:{}_Collecting info time: {:.2f} sec(s)".format(tmpProcessName, tmpProcessId, time.time() - start))

        start = time.time()
        threadList.append(rowValues)
        print("{}:{}_Adding to list time: {:.2f} sec(s)".format(tmpProcessName, tmpProcessId, time.time() - start))

    def main(self):

        startTime = time.time()

        mgr = Manager()
        myList = mgr.list()
        fName = sys.argv[1] + "\*.csv"
        print ("Static file search by, ", fName)
        fileNames = [tmpFileName for tmpFileName in glob.glob(fName)]

        self.processingFile(fileNames, myList)

        start = time.time()
        finalListofList = myList
        finalList = []
        [finalList.extend(tmp) for tmp in finalListofList]
        print("Count of rows: {}".format(len(finalList)))
        print("Count of unique rows: {}".format(len(set(finalList))))
        print("Time taken for counting: {:.2f} sec(s)".format(time.time() - start))
        print("Total script execution time: {:.2f} sec(s)".format(time.time() - startTime))

if __name__ == "__main__":
    csv = CSV_Reading()
    csv.main()